import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rules here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rules extends World
{
    /**
     * Constructor for objects of class Rules.
     * 
     */
    public Rules()
    {           
        super(625, 570, 1); 
        addRules();
    }
    public void play(){
        Greenfoot.setWorld(new Main());
    }
    private void addRules(){
        Instructions rules = new Instructions();
        addObject(rules, 300, 284);
        StartButton start = new StartButton();
        addObject(start, 525, 480);
    }
}
