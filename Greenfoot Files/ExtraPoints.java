import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ExtraPoints here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ExtraPoints extends Actor
{
    /**
     * Act - do whatever the ExtraPoints wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        pickPoint();
    }  
    public void pickPoint(){
        int picIndex = (int)(Math.random() * ((2 - 1) + 1)) + 1;
        setImage(new GreenfootImage("FrogOrPug" + picIndex + ".png"));
    }      
}
