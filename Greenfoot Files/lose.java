import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class lose here.
 *      
 * @author (your name) 
 * @version (a version number or a date)
 */
public class lose extends World
{
    cont conti = new cont();
    Loser los = new Loser();
    GreenfootImage mine = new GreenfootImage("lost2.png");
    /**
     * Constructor for objects of class lose.
     * 
     */
    public lose()
    {    
        super(625, 570, 1); 
        setBackground("graduate.png");
        addObject(conti, 525, 525);
        addObject(los, 110, 500);
    }
    public void act(){
        contin();
    }
    public void play(){
        Greenfoot.setWorld(new Main());
    }
    private void restarter(){
        menu restart = new menu();
        addObject(restart, 525, 480);
    }
    private void contin(){  
        if(Greenfoot.mouseClicked(conti)){
            los.setImage(mine);
            removeObject(conti);
            setBackground("mcde2.png");
            restarter();
        }
    }
}
