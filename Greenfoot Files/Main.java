import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Main here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Main extends World
{
    public static int a;
    public static int JunkFoodCounter;
    public static int time;
    public static int lives;
    SocialMediaCounter socialMediaCounter= new SocialMediaCounter(); 
    JunkFoodCounter junkFoodCounter = new JunkFoodCounter(); 
    LivesCounter livesCounter = new LivesCounter ();  
    Clock clock= new Clock();
   GreenfootSound sound = new GreenfootSound("background.mp3");
    public Main()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(625, 570, 1); 
        a=0;
        JunkFoodCounter=0;
        prepare();
    }
    public void act(){
       int a= Greenfoot.getRandomNumber(500);
        if ( a==1 ){
            addObject(new books(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==2){
            addObject(new word(),Greenfoot.getRandomNumber(624),10);
        }
       else if (a==3){
            addObject(new classroom(), Greenfoot.getRandomNumber(624),10);
        }
        else if (a==4){
            addObject(new sparknotes(), Greenfoot.getRandomNumber(624),10);
        }
         else if (a==5){
            addObject(new chocolate(), Greenfoot.getRandomNumber(624),10);
        }
        else if (a==6){
            addObject(new donut(), Greenfoot.getRandomNumber(624),10);
        }
        else if (a==7){
            addObject(new coffee(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==8){
            addObject(new iceCream(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==9){
            addObject(new pizza(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==10){
            addObject(new cake(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==11){
            addObject(new taco(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==12){
            addObject(new twitter(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==13){
            addObject(new Youtube(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==14){
            addObject(new facebook(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==15){
            addObject(new instagram(),Greenfoot.getRandomNumber(624),10);
        }
        else if (a==16){
            addObject(new snap(),Greenfoot.getRandomNumber(624),10);
        }
     }
    public SocialMediaCounter getSocialMediaCounter(){
        return socialMediaCounter;
    }
    public JunkFoodCounter getJunkFoodCounter(){
        return junkFoodCounter;
    }
    //Show number of lives remaining 
    public LivesCounter getLivesCounter(){
        return livesCounter; 
    }
    //User out of lives, game directs to lose screen 
    public void endloser(){
        Greenfoot.setWorld( new lose());
    }
    

    public void prepare(){
        Cat cat= new Cat();
        addObject(cat, 322,499);
        addObject(clock,546,42);
        heart heart = new heart();
        addObject(heart,456,43);

        addObject(livesCounter,421,39);
        addObject(junkFoodCounter,174,74);
        addObject(socialMediaCounter,144,46);
    }


    public void timeend(){
        Win win = new Win();
         win.addObject(socialMediaCounter, 144,46); 
        win.addObject(junkFoodCounter, 174,74); 
        Greenfoot.setWorld(win);
    }
}
