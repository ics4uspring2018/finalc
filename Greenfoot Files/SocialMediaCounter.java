import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
 //import java.awt.Color;
/**
 * Write a description of class SocialMediaCounter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SocialMediaCounter extends Actor
{
    int a;
    GreenfootImage myImage;
    
    /**
     * Act - do whatever the SocialMediaCounter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       myImage= (new GreenfootImage("Times on Social Media: "+ a, 24, Color.BLACK, new Color(0,0,0,0))); 
        setImage(myImage); 
    }    
    public void addScore(){
         a++; 
         act();
    }
}
