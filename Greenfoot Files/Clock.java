import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
// import java.awt.Color;
/**
 * Write a description of class timer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Clock extends Actor
{
   int Time=3000; 
    /**
     * Act - do whatever the burgerCount wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        setImage(new GreenfootImage("Time: "+Time, 24, Color.BLACK, Color.WHITE)); 
        count();
    }    
    public void count(){
       Time--;
       if ( Time<=0){
        Main main = (Main)getWorld();
        main.timeend(); 
       }
    }
}
