import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
// import java.awt.Color;
/**
 * Write a description of class PoutineCounter here.
 * 
 * @author (your name) 
 * @version (a version numbeor a date)
 */
public class LivesCounter extends Actor
{
    int LivesScore = 10;   
    public void act() 
    {
        setImage(new GreenfootImage("Lives:  "+ LivesScore, 24, Color.WHITE, new Color(0,0,0,0)));
    }    
    public void takeLivesScore(){
       LivesScore--; 
       setImage(new GreenfootImage("Lives:  "+ LivesScore, 24, Color.WHITE, new Color(0,0,0,0))); 
        if (LivesScore <=0){
        Greenfoot.setWorld( new lose());
       }
    }
    public void displayLives(){
        setImage(new GreenfootImage("Lives:  "+ LivesScore, 24, Color.WHITE, new Color(0,0,0,0))); 
    }
    
}
