import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Win here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Win extends World
{
    cont conti = new cont();
    Winner win = new Winner();
    /**
     * Constructor for objects of class Win.
     * 
     */
    public Win()
    {    
        super(625, 570, 1); 
        setBackground("phone.png");
        addObject(conti, 525, 525);
        addObject(win, 110, 525);
    }
    public void act(){
        contin();
    }
    private void restarter(){
        menu restart = new menu();
        addObject(restart, 300, 480);
    }
    private void contin(){      
        if(Greenfoot.mouseClicked(conti)){
            win.change();
            removeObject(conti);
            setBackground("mcde2.png");
            restarter();
        }
    }
}
