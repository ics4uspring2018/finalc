import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Write a description of class Character here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Cat extends Actor
{
    public void act() 
    {
        LeftAndRight(); 
        catches();
    }    

    public void LeftAndRight(){
        if (Greenfoot.isKeyDown("left")){
            setLocation(getX()-4, getY());
        }
        else if(Greenfoot.isKeyDown("right")){
            setLocation(getX()+4, getY());
        }    
    }

    public void catches(){
        if (isTouching(books.class)) {
            Greenfoot.playSound("cat.mp3");
            removeTouching(books.class);
            Main main=(Main)getWorld();
            LivesCounter  livesCounter = main.getLivesCounter (); 
            livesCounter.takeLivesScore();
            livesCounter.displayLives();
        }
        else if(isTouching(word.class)) 
        {
            Greenfoot.playSound("cat.mp3");
            removeTouching(word.class);
            Main main=(Main)getWorld();
            LivesCounter  livesCounter = main.getLivesCounter (); 
            livesCounter.takeLivesScore();
            livesCounter.displayLives();
        }
        else if (isTouching(classroom.class)) 
        {
            Greenfoot.playSound("cat.mp3");
            removeTouching(classroom.class);
            Main main=(Main)getWorld();
            LivesCounter  livesCounter = main.getLivesCounter (); 
            livesCounter.takeLivesScore();
            livesCounter.displayLives();
        }
        else if (isTouching(sparknotes.class)) 
        {
            Greenfoot.playSound("cat.mp3");
            removeTouching(sparknotes.class);
            Main main=(Main)getWorld();
            LivesCounter  livesCounter = main.getLivesCounter (); 
            livesCounter.takeLivesScore();
            livesCounter.displayLives();
        }

        else if (isTouching(chocolate.class)) 
        {
            removeTouching(chocolate.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(donut.class)) 
        {
            removeTouching(donut.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(coffee.class)) 
        {
            removeTouching(coffee.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(iceCream.class)) 
        {
            removeTouching(iceCream.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(pizza.class)) 
        {
            removeTouching(pizza.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(cake.class)) 
        {
            removeTouching(cake.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(taco.class)) 
        {
            removeTouching(taco.class);
            Main main=(Main)getWorld();
            JunkFoodCounter junkFoodCounter= main.getJunkFoodCounter(); 
            junkFoodCounter.addScore();

        }
        else if (isTouching(twitter.class)) 
        {
            removeTouching(twitter.class);
            Main main=(Main)getWorld();
            SocialMediaCounter socialMediaCounter= main.getSocialMediaCounter();
            socialMediaCounter .addScore ();

        }
        else if (isTouching(Youtube.class)) 
        {
            removeTouching(Youtube.class);
            Main main=(Main)getWorld();
            SocialMediaCounter socialMediaCounter= main.getSocialMediaCounter();
            socialMediaCounter .addScore ();

        }
        else if (isTouching(facebook.class)) 
        {
            removeTouching(facebook.class);
            Main main=(Main)getWorld();
            SocialMediaCounter socialMediaCounter= main.getSocialMediaCounter();
            socialMediaCounter .addScore ();

        }
        else if (isTouching(instagram.class)) 
        {
            removeTouching(instagram.class);
            Main main=(Main)getWorld();
            SocialMediaCounter socialMediaCounter= main.getSocialMediaCounter();
            socialMediaCounter .addScore ();

        }
        else if (isTouching(snap.class)) 
        {
            removeTouching(snap.class);
            Main main=(Main)getWorld();
            SocialMediaCounter socialMediaCounter= main.getSocialMediaCounter();
            socialMediaCounter .addScore ();

        }
    }
}

   