import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Win here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Title extends World
{
    GreenfootImage title = new GreenfootImage("main title.png");
    GreenfootImage names = new GreenfootImage("names.png");
    GreenfootSound sound = new GreenfootSound("background.mp3");
    /**
     * Constructor for objects of class Win.
     * 
     */
    public Title()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(625, 570, 1);
        getBackground().drawImage(title, 0, 10);
        getBackground().drawImage(names, 525, 310);
        sound.playLoop();
        prepare();
    }

    public void prepare(){
        PlayButton playbutton = new PlayButton();
        addObject(playbutton,164,504);
        playbutton.setLocation(156,479);
        RulesButton rulesbutton = new RulesButton();
        addObject(rulesbutton,459,486);
        rulesbutton.setLocation(456,473);
        rulesbutton.setLocation(462,476);
        TitleText titletext = new TitleText();
        addObject(titletext,316,113);
        titletext.setLocation(309,113);
        titletext.setLocation(285,319);
        titletext.setLocation(314,307);
        Cat cat = new Cat();
        addObject(cat,319,184);
        cat.setLocation(147,354);
        Cat cat2 = new Cat();
        addObject(cat2,467,362);
        titletext.setLocation(320,199);
        cat2.setLocation(464,357);
        removeObject(cat2);
        cat.setLocation(145,353);
        removeObject(cat);
        titletext.setLocation(319,200);
        removeObject(titletext);
    }

    public void play(){
        Greenfoot.setWorld( new Main());
    }

    public void rule(){
        Greenfoot.setWorld( new Rules());
    }
}
