import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class instagram here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class instagram extends Actor
{
    /**
     * Act - do whatever the instagram wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        setLocation(getX(), getY()+4);
        //If reaches the ground then it is removed 
        if (getY() == 569) 
        {
            Main main = (Main)getWorld();
            main.removeObject(this);
        }
    }    
}
